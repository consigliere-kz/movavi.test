<?php

$commonConfig = require "common.php";
$consoleConfig = [
    'controllerNamespace' => 'app\commands',
];

return array_merge_recursive($commonConfig, $consoleConfig);
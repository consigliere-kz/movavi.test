<?php

namespace app\common\currency;

use app\lib\currency\dto\CurrencyRate;
use app\lib\currency\dto\CurrencyRateRequest;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class RbcRateResource
 * @package app\common\currency
 */
class RbcRateResource extends SharedClientHttpResource
{
    const SOURCE_CBRF = 'cbrf';
    const SOURCE_CASH = 'cash';
    const SOURCE_FOREX = 'forex';

    /**
     * @var string
     */
    protected static $resourceUrl = 'https://cash.rbc.ru/cash/json/converter_currency_rate/';

    /**
     * @var string
     */
    protected static $requestDateFormat = 'Y-m-d';

    /**
     * @var string
     */
    protected static $requestMethod = 'GET';

    /**
     * @var string
     */
    protected $source = self::SOURCE_CBRF;

    /**
     * @var array
     */
    protected static $availableSources = [
        self::SOURCE_CBRF,
        self::SOURCE_CASH,
        self::SOURCE_FOREX
    ];

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @param string $source
     * @return RbcRateResource
     * @throws \InvalidArgumentException
     */
    public function setSource(string $source): RbcRateResource
    {
        if (!in_array($source, static::$availableSources)) {
            throw new \InvalidArgumentException('Invalid source');
        }

        $this->source = $source;

        return $this;
    }

    /**
     * @inheritdoc
     */
    protected function buildHttpRequest(CurrencyRateRequest $request): RequestInterface
    {
        $uri = static::$resourceUrl . '?' . http_build_query($this->getHttpRequestParams($request));

        return new Request(static::$requestMethod, $uri);
    }

    /**
     * @param CurrencyRateRequest $request
     * @return array
     */
    protected function getHttpRequestParams(CurrencyRateRequest $request): array
    {
        $date = $request->getDate();

        return [
            'currency_from' => $request->getSourceCurrency()->getIsoCode(),
            'currency_to' => $request->getTargetCurrency()->getIsoCode(),
            'date' => $date ? $date->format(static::$requestDateFormat) : '',
            'source' => $this->source,
            'sum' => 1,
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getRateFromResponse(ResponseInterface $response, CurrencyRateRequest $request): CurrencyRate
    {
        $currencyRate = new CurrencyRate($request->getSourceCurrency(), $request->getTargetCurrency());

        try {
            $json = json_decode($response->getBody()->getContents(), true);
            $rateValue = $json['data']['rate1'] ?? null;

            if ($rateValue === null) {
                throw new \InvalidArgumentException('No rate data found');
            }

            return $currencyRate->setRate($rateValue);
        } catch (\Exception $e) {
            throw new \InvalidArgumentException('Invalid response content. ' . $e->getMessage());
        }
    }
}
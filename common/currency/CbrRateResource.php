<?php

namespace app\common\currency;

use app\lib\currency\dto\Currency;
use app\lib\currency\dto\CurrencyRate;
use app\lib\currency\dto\CurrencyRateRequest;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use SimpleXMLElement;

/**
 * Class RbcRateResource
 * @package app\common\currency
 */
class CbrRateResource extends SharedClientHttpResource
{
    /**
     * @var string
     */
    protected static $resourceUrl = 'http://www.cbr.ru/scripts/XML_daily.asp';

    /**
     * @var string
     */
    protected static $requestDateFormat = 'd/m/Y';

    /**
     * @var string
     */
    protected static $requestMethod = 'GET';

    /**
     * @inheritdoc
     */
    protected function buildHttpRequest(CurrencyRateRequest $request): RequestInterface
    {
        $this->ensureRequestIsValid($request);
        $uri = static::$resourceUrl . '?' . http_build_query($this->getHttpRequestParams($request));

        return new Request(static::$requestMethod, $uri);
    }

    /**
     * @param CurrencyRateRequest $request
     * @return array
     */
    protected function getHttpRequestParams(CurrencyRateRequest $request): array
    {
        $date = $request->getDate();

        return [
            'date_req' => $date ? $date->format(static::$requestDateFormat) : '',
        ];
    }

    /**
     * @inheritdoc
     */
    protected function getRateFromResponse(ResponseInterface $response, CurrencyRateRequest $request): CurrencyRate
    {
        $currencyRate = new CurrencyRate($request->getSourceCurrency(), $request->getTargetCurrency());

        try {
            $xml = new SimpleXMLElement($response->getBody()->getContents());

            foreach ($xml->Valute as $currency) {
                if ($currency->CharCode == $request->getSourceCurrency()->getIsoCode()) {
                    $rate = (float)str_replace(',', '.', $currency->Value);
                    return $currencyRate->setRate($rate);
                }
            }

            throw new \InvalidArgumentException('No rate data found');
        } catch (\Exception $e) {
            throw new \InvalidArgumentException('Invalid response content. ' . $e->getMessage());
        }
    }

    /**
     * @param CurrencyRateRequest $request
     * @throws \InvalidArgumentException
     */
    protected function ensureRequestIsValid(CurrencyRateRequest $request)
    {
        if ($request->getTargetCurrency()->getIsoCode() !== Currency::ISO_CODE_RUB) {
            throw new \InvalidArgumentException('This resource can accept only RUR as target currency');
        }
    }
}
<?php

namespace app\lib\http;

use Psr\Http\Message\{RequestInterface, ResponseInterface};

/**
 * Interface ClientInterface
 * @package app\lib\http
 */
interface ClientInterface
{
    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function sendRequest(RequestInterface $request): ResponseInterface;
}
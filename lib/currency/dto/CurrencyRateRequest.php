<?php

namespace app\lib\currency\dto;

/**
 * Class CurrencyRateRequest
 * @package app\lib\currency\dto
 */
class CurrencyRateRequest
{
    /**
     * @var Currency
     */
    protected $sourceCurrency;

    /**
     * @var Currency
     */
    protected $targetCurrency;

    /**
     * @var \DateTime|null
     */
    protected $date = null;

    /**
     * CurrencyRateRequest constructor.
     * @param Currency $sourceCurrency
     * @param Currency $targetCurrency
     * @param \DateTime|null $date
     */
    public function __construct(Currency $sourceCurrency, Currency $targetCurrency, \DateTime $date = null)
    {
        $this->sourceCurrency = $sourceCurrency;
        $this->targetCurrency = $targetCurrency;
        $this->date = $date;
    }

    /**
     * @return Currency
     */
    public function getSourceCurrency(): Currency
    {
        return $this->sourceCurrency;
    }

    /**
     * @return Currency
     */
    public function getTargetCurrency(): Currency
    {
        return $this->targetCurrency;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }
}
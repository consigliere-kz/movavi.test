<?php

namespace app\lib\currency\rate;
use app\lib\currency\dto\CurrencyRate;
use app\lib\currency\dto\CurrencyRateRequest;

/**
 * Class ResourceInterface
 */
interface ResourceInterface
{
    /**
     * @param CurrencyRateRequest $request
     * @return CurrencyRate
     * @throws \Exception
     */
    public function getCurrencyRate(CurrencyRateRequest $request): CurrencyRate;
}
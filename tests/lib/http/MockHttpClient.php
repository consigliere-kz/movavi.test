<?php

namespace app\tests\lib\http;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class MockHttpClient
 */
class MockHttpClient implements \app\lib\http\ClientInterface
{
    /**
     * @var string
     */
    protected $responseBody = '';

    /**
     * MockHttpClient constructor.
     * @param string $responseBody
     */
    public function __construct(string $responseBody)
    {
        $this->responseBody = $responseBody;
    }

    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        return new \GuzzleHttp\Psr7\Response(200, [], $this->responseBody);
    }
}
<?php

use \app\lib\currency\dto\Currency;
use \app\lib\currency\dto\CurrencyRateRequest;
use \app\lib\currency\dto\CurrencyRate;
use \app\lib\currency\rate\AvgCompositeResource;
use \app\lib\currency\rate\ResourceInterface;

class AvgRateResourceTest extends \yii\codeception\TestCase
{
    /**
     * @covers AvgCompositeResource::getCurrencyRate()
     */
    public function testAllResourcesAvailable()
    {
        $resourcesPool = [
            $this->getFixedResource(50),
            $this->getFixedResource(100),
            $this->getFixedResource(75)
        ];
        $compositeResource = new AvgCompositeResource(...$resourcesPool);
        $rateRequest = new CurrencyRateRequest(
            new Currency(),
            new Currency()
        );
        $resourceRate = $compositeResource->getCurrencyRate($rateRequest)->getRate();

        $this->assertEquals(75, $resourceRate);
    }

    /**
     * @expectedException \Exception
     */
    public function testOneResourceUnavailable()
    {
        $resourcesPool = [
            $this->getUnavailableResource(),
            $this->getFixedResource(100),
            $this->getFixedResource(75)
        ];
        $compositeResource = new AvgCompositeResource(...$resourcesPool);
        $rateRequest = new CurrencyRateRequest(
            new Currency(),
            new Currency()
        );
        $compositeResource->getCurrencyRate($rateRequest);
    }

    /**
     * @return ResourceInterface
     */
    protected function getUnavailableResource()
    {
        return new class implements ResourceInterface {
            public function getCurrencyRate(CurrencyRateRequest $request): CurrencyRate
            {
                throw new \Exception('Can\'t get rate data');
            }
        };
    }

    /**
     * @param $rateValue
     * @return mixed
     */
    protected function getFixedResource(float $rateValue)
    {
        return new class($rateValue) implements ResourceInterface {
            protected $rate;

            public function __construct(float $rate)
            {
                $this->rate = $rate;
            }


            public function getCurrencyRate(CurrencyRateRequest $request): CurrencyRate
            {
                return (new CurrencyRate(
                    $request->getSourceCurrency(),
                    $request->getTargetCurrency()
                ))->setRate($this->rate);
            }
        };
    }
}